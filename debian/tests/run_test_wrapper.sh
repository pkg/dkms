#!/bin/sh

KERNEL_VER=
for kver in $(dpkg-query -W -f '${Package}\n' 'linux-headers-*' | sed s/linux-headers-//)
do
	if [ -d "/lib/modules/$kver/build" ]
	then
		KERNEL_VER=$kver
		break
	fi
done

export KERNEL_VER
bash ./run_test.sh
